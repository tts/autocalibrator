#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <algorithm>
#include <unordered_map>
#include <iomanip>
#include <vector>
#include <cstring>
#include <math.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>
#include <sys/file.h>
#include <X11/extensions/Xrandr.h>
#include <X11/extensions/XInput2.h>
#include <X11/Xatom.h>

typedef struct {
    uint16_t width;
    uint16_t height;
    int32_t  axys[4];
    std::string touch_name;
    bool swapxy;
    bool invertx;
    bool inverty;
} monitorSection_t;

typedef enum {
    ScalingMode_None = 0,
    ScalingMode_Full,
    ScalingMode_Center,
    ScalingMode_Full_aspect
} ScalingMode;

bool isNumber(const std::string& str)
{
    for (char const &c : str) {
        if (std::isdigit(c) == 0) return false;
    }
    return true;
}

int search_touchscreen_device(Display *display, std::string& devname) {
    XIDeviceInfo     *info  = NULL;
    XIDeviceInfo     *dev   = NULL;
    XITouchClassInfo *touch = NULL;

    int i, j, ndevices;
    int result = (-1);

    if (devname.length() > 0) {
        info = XIQueryDevice(display, XIAllDevices, &ndevices);
        if (isNumber(devname)) { 
            int deviceid = std::stoi(devname);
            for (i = 0; i < ndevices; i++) {
                dev = info + i;
                if (deviceid ==  dev->deviceid) {
                   result = deviceid;
                   break;
                }
            }
        } else {
            for (i = 0; i < ndevices; i++) {
                dev = info + i;
                if (!std::strcmp(devname.c_str(), dev->name)) {
                   result = dev->deviceid;
                   break;
                }
            }
        }
        XIFreeDeviceInfo(info);
    }
    return result;
}


static int apply_matrix(Display *dpy, int deviceid, float m[]) {
    Atom prop_float, prop_matrix;

    union {
        unsigned char *c;
        float *f;
    } data;

    int format_return;
    Atom type_return;
    unsigned long nitems;
    unsigned long bytes_after;

    int rc, i;

    prop_float = XInternAtom(dpy, "FLOAT", False);
    prop_matrix = XInternAtom(dpy, "Coordinate Transformation Matrix", False);

    if (!prop_float) {
        std::cerr << "Float atom not found. This server is too old." << std::endl;
        return EXIT_FAILURE;
    }
    if (!prop_matrix) {
        std::cerr << "Coordinate transformation matrix not found. This "
                "server is too old" << std::endl;
        return EXIT_FAILURE;
    }

    rc = XIGetProperty(dpy, deviceid, prop_matrix, 0, 9, False, prop_float,
                       &type_return, &format_return, &nitems, &bytes_after,
                       &data.c);
    if (rc != Success || prop_float != type_return || format_return != 32 ||
        nitems != 9 || bytes_after != 0) {
        std::cerr << "Failed to retrieve current property values" << std::endl;
        return EXIT_FAILURE;
    }

    for (i = 0; i < 9; i++) data.f[i] = m[i];

    XIChangeProperty(dpy, deviceid, prop_matrix, prop_float,
                     format_return, PropModeReplace, data.c, nitems);

    XFree(data.c);
    fprintf(stdout, "Apply matrix: (%f %f %f %f %f %f %f %f %f) to device %u\n", m[0], m[1], m[2], m[3], m[4], m[5], m[6], m[7], m[8], deviceid);
    return EXIT_SUCCESS;
}

static int apply_axys(Display *dpy, int deviceid, int32_t arrCalibData[], bool swapxy, bool invertx, bool inverty) {
    Atom prop_calibration = XInternAtom(dpy, "Evdev Axis Calibration", False);

    if (!prop_calibration) {
        std::cerr << "Evdev axis calibration not found. This "
                "server is too old" << std::endl;
        return EXIT_FAILURE;
    }

    fprintf(stdout, "Apply evdev axis: (%d %d %d %d) to device %u\n", arrCalibData[0], arrCalibData[1], arrCalibData[2], arrCalibData[3], deviceid);
    XIChangeProperty(dpy, deviceid, prop_calibration, XA_INTEGER,
                     sizeof(uint32_t)*8, PropModeReplace, reinterpret_cast<unsigned char*>(arrCalibData), 4);

    prop_calibration = XInternAtom(dpy, "Evdev Axes Swap", False);
    if (prop_calibration) {
       unsigned char swappiness = (swapxy ? 1 : 0);
       XIChangeProperty(dpy, deviceid, prop_calibration, XA_INTEGER,
                     8, PropModeReplace, &swappiness, 1);
    }
    prop_calibration = XInternAtom(dpy, "Evdev Axis Inversion", False);
    if (prop_calibration) {
       unsigned char inversion[2];
       inversion[0] = (invertx ? 1 : 0);
       inversion[1] = (inverty ? 1 : 0);
       XIChangeProperty(dpy, deviceid, prop_calibration, XA_INTEGER,
                     8, PropModeReplace, inversion, 2);
    }

    XSync(dpy, 0);
    return EXIT_SUCCESS;
}


static void duplicate(float a[], float b[]) {
    int i;
    for (i = 0; i < 9; i++) b[i] = a[i];
}

static void multiply(float a[], float b[], float c[]) {
    float m[9] = {0};
    m[0] = a[0] * b[0] + a[1] * b[3] + a[2] * b[6];
    m[1] = a[0] * b[1] + a[1] * b[4] + a[2] * b[7];
    m[2] = a[0] * b[2] + a[1] * b[5] + a[2] * b[8];
    m[3] = a[3] * b[0] + a[4] * b[3] + a[5] * b[6];
    m[4] = a[3] * b[1] + a[4] * b[4] + a[5] * b[7];
    m[5] = a[3] * b[2] + a[4] * b[5] + a[5] * b[8];
    m[6] = a[6] * b[0] + a[7] * b[3] + a[8] * b[6];
    m[7] = a[6] * b[1] + a[7] * b[4] + a[8] * b[7];
    m[8] = a[6] * b[2] + a[7] * b[5] + a[8] * b[8];
    duplicate(m, c);
}

static void rotate_reflect(float m[], Rotation rotation, int &dw, int &dh) {
    float rotate90[] = {
        0   , -1.0f, 1.0f,
        1.0f, 0    , 0,
        0   , 0    , 1.0f
    };
    float rotate180[] = {
        -1.0f, 0    , 1.0f,
        0    , -1.0f, 1.0f,
        0    , 0    , 1.0f
    };
    float rotate270[] = {
        0    , 1.0f, 0,
        -1.0f, 0   , 1.0f,
        0    , 0   , 1.0f
    };
    float reflectX[] = {
        -1.0f, 0   , 1.0f,
        0    , 1.0f, 0,
        0    , 0   , 1.0f
    };
    float reflectY[] = {
        1.0f, 0    , 0,
        0   , -1.0f, 1.0f,
        0   , 0    , 1.0f
    };

    float t[9] = {
        1.0f, 0   , 0,
        0   , 1.0f, 0,
        0   , 0   , 1.0f
    };

    if (rotation & RR_Rotate_90) {
        dw ^= dh;
        dh ^= dw;
        dw ^= dh;
        duplicate(rotate90, t);
    } else if (rotation & RR_Rotate_180) {
        duplicate(rotate180, t);
    } else if (rotation & RR_Rotate_270) {
        dw ^= dh;
        dh ^= dw;
        dw ^= dh;
        duplicate(rotate270, t);
    }

    if ((rotation & RR_Rotate_0) || (rotation & RR_Rotate_180)) {
        if (rotation & RR_Reflect_X) {
            multiply(t, reflectX, t);
        }
        if (rotation & RR_Reflect_Y) {
            multiply(t, reflectY, t);
        }
    } else {
        if (rotation & RR_Reflect_X) {
            multiply(t, reflectY, t);
        }
        if (rotation & RR_Reflect_Y) {
            multiply(t, reflectX, t);
        }
    }

    multiply(m, t, m);
}

// sw - screen width
// sh - screen height
// dw - display width
// dh - display height
// dx - display x
// dy - display y
// pw - preferred width
// ph - preferred height

void scaling_full_mode(float *m, Rotation rotation, int dx, int dy, int dw, int dh, int sw, int sh, int pw, int ph) {
    float shift[9] = {
        1.0f , 0    , 1.0f * dx / sw ,
        0    , 1.0f , 1.0f * dy / sh ,
        0    , 0    , 1.0f
    };
    float zoom[9] = {
        1.0f * dw / sw , 0              , 0 ,
        0              , 1.0f * dh / sh , 0 ,
        0              , 0              , 1.0f
    };
    std::memset(m, 0, 9);
    multiply(shift, zoom, m);
    rotate_reflect(m, rotation, dw, dh);
}

void scaling_center_mode(float *m, Rotation rotation, int dx, int dy, int dw, int dh, int sw, int sh, int pw, int ph) {
    float shift1[9] = {
        1.0f , 0    , 1.0f * dx / sw ,
        0    , 1.0f , 1.0f * dy / sh ,
        0    , 0    , 1.0f
    };
    float zoom1[9] = {
        1.0f * dw / sw , 0              , 0 ,
        0              , 1.0f * dh / sh , 0 ,
        0              , 0              , 1.0f
    };
    float m1[9] = {0};
    multiply(shift1, zoom1, m1);
    rotate_reflect(m1, rotation, dw, dh);
    float zoom2[9] = {
        1.0f * pw / dw , 0              , 0 ,
        0              , 1.0f * ph / dh , 0 ,
        0              , 0              , 1.0f
    };
    float shift2[9] = {
        1.0f , 0    , - (1.0f - 1.0f * dw / pw) / 2 ,
        0    , 1.0f , - (1.0f - 1.0f * dh / ph) / 2 ,
        0    , 0    , 1.0f
    };
    float m2[9] = {0};
    multiply(zoom2, shift2, m2);
    std::memset(m, 0, 9);
    multiply(m1, m2, m);
}

void scaling_full_aspect_mode(float *m, Rotation rotation, int dx, int dy, int dw, int dh, int sw, int sh, int pw, int ph) {
    float shift1[9] = {
        1.0f , 0    , 1.0f * dx / sw ,
        0    , 1.0f , 1.0f * dy / sh ,
        0    , 0    , 1.0f
    };
    float zoom1[9] = {
        1.0f * dw / sw , 0              , 0 ,
        0              , 1.0f * dh / sh , 0 ,
        0              , 0              , 1.0f
    };
    float m1[9] = {0};
    multiply(shift1, zoom1, m1);
    rotate_reflect(m1, rotation, dw, dh);
    float zoom2[9] = {
        1.0f * pw * dh / ph / dw , 0    , 0 ,
        0                        , 1.0f , 0 ,
        0                        , 0    , 1.0f
    };
    float shift2[9] = {
        1.0f , 0    , (1.0f * ph * dw / pw / dh - 1.0f) / 2 ,
        0    , 1.0f , 0 ,
        0    , 0    , 1.0f
    };
    float m2[9] = {0};
    multiply(zoom2, shift2, m2);
    std::memset(m, 0, 9);
    multiply(m1, m2, m);
}

void scaling_none_mode(float *m, Rotation rotation, int dx, int dy, int dw, int dh, int sw, int sh) {
    float m1[9] = {
        1.0f * dw / sw,  0  , 1.0f * dx / sw,
        0   , 1.0f * dh / sh , 1.0f * dy / sh,
        0   , 0    , 1.0f
    };
    std::memcpy(m, m1, sizeof(m1));
    rotate_reflect(m, rotation, dw, dh);
}

const char* ws = " \t\n\r\f\v";

inline std::string& rtrim(std::string& s, const char* t = ws) {
    s.erase(s.find_last_not_of(t) + 1);
    return s;
}

inline std::string& ltrim(std::string& s, const char* t = ws) {
    s.erase(0, s.find_first_not_of(t));
    return s;
}

inline std::string& trim(std::string& s, const char* t = ws) {
    return ltrim(rtrim(s, t), t);
}

std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    std::stringstream ss;
    ss.str(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

std::unordered_map<std::string, monitorSection_t> monitors;

static void search_monitors(Display *dpy) {
    XRRMonitorInfo      *m;
    int                 n;
    Window              root;
    int                 event_base, error_base;
    SizeID              current_size;
    Rotation            current_rotation;

    XRRScreenResources *sr;
    XRRCrtcInfo *ci = NULL;

    int screen = DefaultScreen (dpy);
    root = RootWindow (dpy, screen);
    int sw = DisplayWidth(dpy, screen);
    int sh = DisplayHeight(dpy, screen);

    if (!XRRQueryExtension (dpy, &event_base, &error_base)) {
        return;
    }
    sr = XRRGetScreenResources(dpy, root);

    for (int j = 0; j < sr->noutput; j++) {
        XRROutputInfo *outinfo = XRRGetOutputInfo (dpy, sr, sr->outputs[j]);
        if (outinfo->crtc != 0) {
           std::string outname = outinfo->name;
           std::unordered_map<std::string, monitorSection_t>::iterator iter = monitors.find(outname);
           if (iter != monitors.end()) {
                ci = XRRGetCrtcInfo(dpy, sr, outinfo->crtc);
                ScalingMode scaling_mode = ScalingMode_None;

                int pw = ci->width;
                int ph = ci->height;
                for (int n = 0; n < outinfo->nmode; n++) {
                    if (n < outinfo->npreferred) {
                        RRMode *mode = &outinfo->modes[n];
                        for (int k = 0; k < sr->nmode; k++) {
                            XRRModeInfo *mode_info = &sr->modes[k];
                            if (mode_info->id == *mode) {
                                int nprop = 0;
                                pw = mode_info->width;
                                ph = mode_info->height;
                                Atom *props = XRRListOutputProperties (dpy, sr->outputs[j], &nprop);
                                for (int l = 0; l < nprop; l++) {
                                    if (strcmp("scaling mode", XGetAtomName (dpy, props[l])) == 0) {
                                        unsigned char *prop;
                                        int actual_format;
                                        unsigned long nitems, bytes_after;
                                        Atom actual_type;
                                        XRRGetOutputProperty (dpy, sr->outputs[j], props[l],
                                                0, 100, False, False,
                                                AnyPropertyType,
                                                &actual_type, &actual_format,
                                                &nitems, &bytes_after, &prop);
                                        if (actual_type == XA_ATOM &&
                                            actual_format == 32 &&
                                            nitems == 1 &&
                                            bytes_after == 0) {
                                            const char* name = XGetAtomName (dpy, ((Atom *)prop)[0]);
                                            if (strcmp("None", name) == 0) {
                                                scaling_mode = ScalingMode_None;
                                            } else if (strcmp("Full", name) == 0) {
                                                scaling_mode = ScalingMode_Full;
                                            } else if (strcmp("Center", name) == 0) {
                                                scaling_mode = ScalingMode_Center;
                                            } else if (strcmp("Full aspect", name) == 0) {
                                                scaling_mode = ScalingMode_Full_aspect;
                                            }
                                            goto next;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
next:
                int rot_angle;

                switch (ci->rotation) {
                    case RR_Rotate_0:
                       rot_angle = 0;
                       break;
                    case RR_Rotate_90:
                       rot_angle = 90;
                       break;
                    case RR_Rotate_180:
                       rot_angle = 180;
                       break;
                    case RR_Rotate_270:
                       rot_angle = 270;
                       break;
                    default:
                       rot_angle = 1;
                       break;
                }
                monitorSection_t monInfo = monitors[outname];
                fprintf(stdout, "Set %s mode: %ux%u+%u+%u pw:%ux%u screen:%ux%u rotate:%u\x0a\n", outname.c_str(), ci->width, ci->height, ci->x, ci->y, pw, ph, sw, sh, rot_angle);

                int deviceid = search_touchscreen_device(dpy, monInfo.touch_name);
                if (deviceid >= 0) {
                    float m[9];

                    switch (scaling_mode) {
                        case ScalingMode_None:
                            std::cout << "Select transformation without scaling..." << std::endl;
                            scaling_none_mode(m, ci->rotation, ci->x, ci->y, ci->width, ci->height, sw, sh);
                        break;
                        case ScalingMode_Full:
                            std::cout << "Select transformation with full mode scaling..." << std::endl;
                            scaling_full_mode(m, ci->rotation, ci->x, ci->y, ci->width, ci->height, sw, sh, pw, ph);
                        break;
                        case ScalingMode_Center:
                            std::cout << "Select transformation with center mode scaling..." << std::endl;
                            scaling_center_mode(m, ci->rotation, ci->x, ci->y, ci->width, ci->height, sw, sh, pw, ph);
                        break;
                        case ScalingMode_Full_aspect:
                            std::cout << "Select transformation with full aspect scaling..." << std::endl;
                            scaling_full_aspect_mode(m, ci->rotation, ci->x, ci->y, ci->width, ci->height, sw, sh, pw, ph);
                        break;
                    }
                    apply_matrix(dpy, deviceid, m);
                } else
                     std::cout << "Touchscreen device with ID \"" << monInfo.touch_name << "\"not found." << std::endl;
           }
        }
    }
}

void routine(Display **display) {
    XCloseDisplay(*display);
    usleep(100000);
    *display = XOpenDisplay(getenv("DISPLAY"));
    search_monitors(*display);
    XRRSelectInput(*display, RootWindow(*display, 0), RROutputChangeNotifyMask);
    XSelectInput(*display, RootWindow(*display, 0), StructureNotifyMask);
    XSync(*display, False);
}

static std::string config_file = "/opt/current_config.sh";

bool get_config(std::string& cnffile, Display *display) {
    std::string sectValue;

    if (std::ifstream configFileStream{ cnffile }; configFileStream) {
        std::unordered_map<std::string, std::string> configData;
        for (std::string line{}; std::getline(configFileStream, line);) {
            if (line.find('=') != std::string::npos) {
                std::istringstream iss{ line };
                if (std::string id{}, value{}; std::getline(std::getline(iss, id, '=') >> std::ws, value)) {
                    configData[id] = trim(value);
                }
            }
        }
        for(int x = 1; x < 6; x++) {
            std::string mSection = "MON" + std::to_string(x);
            std::unordered_map<std::string, std::string>::iterator iter = configData.find(mSection + "_OUTPUT");
            if (iter != configData.end()) {
                std::string monName = configData[mSection + "_OUTPUT"];
                sectValue = configData[mSection + "_TOUCH"];
                if (sectValue.length() > 0) {
                    monitorSection_t monDef = { 0, 0, 0, 0, 0, 0, "", false, false, false };
                    sectValue.erase(std::remove(sectValue.begin(), sectValue.end(), '\"'), sectValue.end());
                    monDef.touch_name.assign(sectValue);
                    sectValue = configData[mSection + "_AXYS"];
                    if (sectValue.length() > 0) {
                        if (sectValue.front() == '"' ) {
                           sectValue.erase( 0, 1 );
                           sectValue.erase( sectValue.size() - 1 );
                        }
                        std::vector<std::string> parts = split(sectValue, ',');
                        if (parts.size() >= 4) {
                           for (int i = 0; i < 4; i++) {
                               monDef.axys[i] = std::stoi(parts[i]);
                           }
                        }
                        if (parts.size() > 4) {
                           monDef.swapxy = (parts[4] == "true" ? true : false);
                        }
                        if (parts.size() > 6) {
                           monDef.invertx = (parts[5] == "true" ? true : false);
                           monDef.inverty = (parts[6] == "true" ? true : false);
                        }
                    }
                    std::cout << "Add monitor " << monName << " with touch name " << monDef.touch_name << std::endl;
                    monitors[monName] = monDef;
                    int deviceid = search_touchscreen_device(display, monDef.touch_name);
                    if ((deviceid >= 0) &&
                        (monDef.axys[0] != 0 || monDef.axys[1] != 0 || monDef.axys[2] != 0 || monDef.axys[3] != 0)) {
                          apply_axys(display, deviceid, monDef.axys, monDef.swapxy, monDef.invertx, monDef.inverty);
                    }
                }
            }
        }
        return true;
    } else {
        std::cerr << "Config file " << config_file << " not found in search paths." << std::endl;
        return false;
    }
}

Display *display = NULL;

const char *pid_filename = "/var/run/autocalibrator.lock";
int pid_file;

void user_sig_handler (int signum) {
    if (signum == SIGUSR1) {
        std::cout << "Received signal SIGUSR1, trying to refresh configuration..." << std::endl;
        get_config(config_file, display);
    } else {
       close(pid_file);
       unlink(pid_filename);
       std::cout << "Normal programm stopping." << std::endl;
       exit(0);
    }
}


static void usage(char* cmd)
{
    std::cerr << "Usage: " << cmd << " [-h|--help] [-c|--config] [-d|--daemon]" << std::endl;
    std::cerr << "\t-h, --help: print this help message" << std::endl;
    std::cerr << "\t-c, --config: use non-standart location of configuration file (default: /opt/current_config.sh)" << std::endl;
    std::cerr << "\t-d, --daemon: run programm in background mode like a daemon" << std::endl;
    exit(0);
}

int main(int argc, char *argv[]) {
    int event_base, error_base;
    int major, minor;

    int flag_daemon = 0;
    const char* short_opts = "hc:d";
    static struct option long_opts[] = {
                    { "help", no_argument, 0, 'h'},
                    { "config", required_argument, 0, 'c'},
                    { "daemon", no_argument, 0, 'd'},
                    { 0, 0, 0, 0 }
    };
    int option_index = (-1), ch;
    while ((ch = getopt_long(argc, argv, short_opts, long_opts, &option_index)) != -1) {
       switch (ch) {
         case 'c':
             if (optarg)
                config_file = optarg; 
             else {
                std::cerr << "Required argument: configuration file name." << std::endl;
                usage(argv[0]);
             }
             break;
         case 'd':
             flag_daemon = 1;
             break;
         case 'h':
             usage(argv[0]);
             break;
         case '?': default:
             usage(argv[0]);
             break;
       }
       option_index = (-1);
    }

    int pid_file = open(pid_filename, O_CREAT | O_RDWR, 0666);
    int rc = flock(pid_file, LOCK_EX | LOCK_NB);
    if (rc) {
         if(EWOULDBLOCK == errno)
            std::cerr << "Only one instance of programm must be launched!" << std::endl;
         else
            std::cerr << "Error launch programm. You must be a root!" << std::endl;
         exit(0);
    }
    display = XOpenDisplay(getenv("DISPLAY"));

    if (display == NULL) {
        std::cerr << "Unable to connect to X server" << std::endl;
        return EXIT_FAILURE;
    }

    if (!XRRQueryExtension (display, &event_base, &error_base) ||
        !XRRQueryVersion (display, &major, &minor)) {
        std::cerr << "No XRandr extensions!" << std::endl;
        XCloseDisplay(display);
        return EXIT_FAILURE;
    }
    if (flag_daemon && daemon(0, 0) != 0) {
        std::cerr << "Unable to start daemon!" << std::endl;
        XCloseDisplay(display);
        return EXIT_FAILURE;
    }

    usleep(100000);
    if (!get_config(config_file, display)) {
        std::cerr << "Config file " << config_file << " not found in search paths." << std::endl;
        return EXIT_FAILURE;
    }
    routine(&display);

    if (signal (SIGUSR1, user_sig_handler) == SIG_IGN)
        signal (SIGUSR1, SIG_IGN);
    if (signal (SIGINT, user_sig_handler) == SIG_IGN)
        signal (SIGINT, SIG_IGN);
    if (signal (SIGTERM, user_sig_handler) == SIG_IGN)
        signal (SIGTERM, SIG_IGN);

    for (;;) {
        XEvent ev;
        XRRNotifyEvent *nev;

        do {
            XNextEvent(display, &ev);
            switch (ev.type - event_base) {
                case RRNotify:
                    nev = (XRRNotifyEvent *) &ev;
                    if (nev->subtype == RRNotify_OutputChange) {
                        routine(&display);
                    }
                    break;
            }
            switch (ev.type) {
                case ConfigureNotify:
                    routine(&display);
                    break;
            }
        } while (XPending(display));
    }
    close(pid_file);
    unlink(pid_filename);
    XCloseDisplay(display);
    return EXIT_SUCCESS;
}
