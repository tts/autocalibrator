# autocalibrator

Программа запускается после старта X-сервера. 
При первом запуске проверяет текущие настройки, и если к устройствам вывода применены какие-нибудь коэффициенты трансформации (поворот, отражение, масштабирование, сдвиг), рассчитывает матрицы трансформации для устройств ввода, и применяет их. 
В дальнейшем программа остается в памяти и следит за изменениями экрана. В каждом случае применения трансформации к экрану, она применяет коэффициенты к устройствам ввода.
Соответствие устройств ввода и вывода берется из файла /opt/current_config.sh. Файл можно переназначить при помощи ключа -c. Так же из этого файла берутся и применяются Данные калибровки. Если в файле изменились данные, можно заставить программу перечитать настройки, послав сигнал SIGUSR1.

Перечень параметров в файле конфигурации:

MON[#]=OUTNAME  - наименование устройства вывода для экрана с заданным номером, например: MON1=DVI-I-1
MON[#]_TOUCH=DEVID — идентификатор устройства ввода, соответствующего монитору с заданным номером. Может цифровым идентификатором устройства, а также именем устройства, как оно отображается в списке xinput. Например: MON1_TOUCH=15 или MON1_TOUCH="ELAN Touchscreen".
MON[#]_TOUCHAXYS — перечень значений осей калибровки тач-скрина, разделенный двоеточием. Например: MON1_TOUCHAXYS="0,23767,0,32767,0,false,false,false".  Последние 3 параметра — поменять оси X и Y местами (иногда случается на мелких тачах), инвертировать ось X, инвертировать ось Y. Если 1 — поменять местами.

Соответственно, если коэффициенты к устройствам ввода применяются сразу после применения трансформации геометрии экрана, то калибровать эти устройства можно сразу после этого. Калибратор будет получать координаты мыши так, будто он работает на единственном экране в своем оригинальном неперевернутом режиме. В связи с эти также пришлось опять убрать из патча xinput_calibrator добавление координат к щелчку мыши, т. к. в этом случае координаты уже пересчитаны.

С ключом -d уходит в режим демона.  Без ключа можно отлаживаться. На экран должны выводиться сообщение при применении параметров экрана.

## Build from source

### Prerequisite

    autoconf >= 2.68
    automake >= 1.11
    pkg-config >= 0.26
    libtool >= 2.4.2

## Coordinate Transformation Matrix

### Zoom

Ограничение перемещения курсора в пределах левой половины сенсорного дисплея.

![my equation](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D%5Cbegin%7BBmatrix%7D0.5%20&%200%20&%200%20%5C%5C0%20&%201%20&%200%20%5C%5C0%20&%200%20&%201%20%5C%5C%5Cend%7BBmatrix%7D)

$ xinput set-prop '&lt;device name&gt;' 'Coordinate Transformation Matrix' 0.5 0 0 0 1 0 0 0 1

Ограничение перемещения курсора в пределах верхней половины сенсорного дисплея.

![my equation](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D%5Cbegin%7BBmatrix%7D1%20&%200%20&%200%20%5C%5C0%20&%200.5%20&%200%20%5C%5C0%20&%200%20&%201%20%5C%5C%5Cend%7BBmatrix%7D)

$ xinput set-prop '&lt;device name&gt;' 'Coordinate Transformation Matrix' 1 0 0 0 0.5 0 0 0 1

Map the cursor movement to the left half touchscreen area.

![my equation](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D%5Cbegin%7BBmatrix%7D2%20&%200%20&%200%20%5C%5C0%20&%201%20&%200%20%5C%5C0%20&%200%20&%201%20%5C%5C%5Cend%7BBmatrix%7D)

$ xinput set-prop '&lt;device name&gt;' 'Coordinate Transformation Matrix' 2 0 0 0 1 0 0 0 1

Map the cursor movement to the top half touchscreen area.

![my equation](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D%5Cbegin%7BBmatrix%7D1%20&%200%20&%200%20%5C%5C0%20&%202%20&%200%20%5C%5C0%20&%200%20&%201%20%5C%5C%5Cend%7BBmatrix%7D)

$ xinput set-prop '&lt;device name&gt;' 'Coordinate Transformation Matrix' 1 0 0 0 2 0 0 0 1

### Translation

Make the cursor shift right by half the width of the touchscreen.

![my equation](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D%5Cbegin%7BBmatrix%7D1%20&%200%20&%200.5%20%5C%5C0%20&%201%20&%200%20%5C%5C0%20&%200%20&%201%20%5C%5C%5Cend%7BBmatrix%7D)

$ xinput set-prop '&lt;device name&gt;' 'Coordinate Transformation Matrix' 1 0 0.5 0 1 0 0 0 1

Make the cursor shift left by half the width of the touchscreen.

![my equation](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D%5Cbegin%7BBmatrix%7D1%20&%200%20&%20-0.5%20%5C%5C0%20&%201%20&%200%20%5C%5C0%20&%200%20&%201%20%5C%5C%5Cend%7BBmatrix%7D)

$ xinput set-prop '&lt;device name&gt;' 'Coordinate Transformation Matrix' 1 0 -0.5 0 1 0 0 0 1

Make the cursor shift down by half the height of the touchscreen.

![my equation](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D%5Cbegin%7BBmatrix%7D1%20&%200%20&%200%20%5C%5C0%20&%201%20&%20-0.5%20%5C%5C0%20&%200%20&%201%20%5C%5C%5Cend%7BBmatrix%7D)

$ xinput set-prop '&lt;device name&gt;' 'Coordinate Transformation Matrix' 1 0 0 0 1 0.5 0 0 1

Make the cursor shift up by half the height of the touchscreen.

![my equation](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D%5Cbegin%7BBmatrix%7D1%20&%200%20&%200%20%5C%5C0%20&%201%20&%20-0.5%20%5C%5C0%20&%200%20&%201%20%5C%5C%5Cend%7BBmatrix%7D)

$ xinput set-prop '&lt;device name&gt;' 'Coordinate Transformation Matrix' 1 0 0 0 1 -0.5 0 0 1

### Zoom x Translation

For example, if we want the center 1/4 area of the touchscreen to operate the fullscreen cursor movement.

![my equation](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D%5Cbegin%7BBmatrix%7D2%20&%200%20&%200%20%20%5C%5C0%20&%201%20&%200%20%5C%5C0%20&%200%20&%201%5Cend%7BBmatrix%7D*%5Cbegin%7BBmatrix%7D1%20&%200%20&%200%20%5C%5C0%20&%202%20&%200%20%5C%5C0%20&%200%20&%201%5Cend%7BBmatrix%7D*%5Cbegin%7BBmatrix%7D1%20&%200%20&%20-0.25%20%5C%5C0%20&%201%20&%200%20%5C%5C0%20&%200%20&%201%20%5Cend%7BBmatrix%7D*%5Cbegin%7BBmatrix%7D1%20&%200%20&%200%20%5C%5C0%20&%201%20&%20-0.25%20%5C%5C0%20&%200%20&%201%5Cend%7BBmatrix%7D)

equals to

![my equation](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D%5Cbegin%7BBmatrix%7D2%20&%200%20&%200%20%5C%5C0%20&%202%20&%200%20%5C%5C0%20&%200%20&%201%20%5C%5C%5Cend%7BBmatrix%7D*%5Cbegin%7BBmatrix%7D1%20&%200%20&%20-0.25%20%5C%5C0%20&%201%20&%20-0.25%20%5C%5C0%20&%200%20&%201%20%5C%5C%5Cend%7BBmatrix%7D)

equals to

![my equation](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D%5Cbegin%7BBmatrix%7D2%20&%200%20&%20-0.5%20%5C%5C0%20&%202%20&%20-0.5%20%5C%5C0%20&%200%20&%201%20%5C%5C%5Cend%7BBmatrix%7D)

$ xinput set-prop '&lt;device name&gt;' 'Coordinate Transformation Matrix' 2 0 -0.5 0 2 -0.5 0 0 1

## Scaling mode

The touchscreen display may support four different modes, such as 'None', 'Full', 'Center' and 'Full aspect'.

    pw = preferred width
    ph = preferred height
    (The preferred resolution of touchscreen display.)

    sw = screen width
    sh = screen height
    (The virtual screen of X Window System.)

    dw = display width
    dh = display height
    dx = display X axis
    dy = display Y axis
    (The actual resolution and position of touchscreen display.)

### 'None' mode

The behavior is undefined. It may work like 'Full' mode or 'Center' mode.

### 'Full' mode

![my equation](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D%5Cbegin%7BBmatrix%7Ddw/sw%20&%200%20&%20dx/sw%20%5C%5C0%20&%20dh/sh%20&%20dy/sh%20%5C%5C0%20&%200%20&%201%20%5C%5C%5Cend%7BBmatrix%7D)

equals to

![my equation](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D%5Cbegin%7BBmatrix%7D1%20&%200%20&%20dx/sw%20%5C%5C0%20&%201%20&%20dy/sh%20%5C%5C0%20&%200%20&%201%20%5C%5C%5Cend%7BBmatrix%7D*%5Cbegin%7BBmatrix%7Ddw/sw%20&%200%20&%200%20%5C%5C0%20&%20dh/sh%20&%200%20%5C%5C0%20&%200%20&%201%20%5C%5C%5Cend%7BBmatrix%7D)


### 'Center' mode

![my equation](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D%5Cbegin%7BBmatrix%7Dpw/dw%20&%200%20&%20(dw-pw)/sw/2&plus;dx/sw%20%5C%5C0%20&%20ph/sh%20&%20(dh-ph)/sh/2&plus;dy/sh%20%5C%5C0%20&%200%20&%201%20%5C%5C%5Cend%7BBmatrix%7D)

equals to

![my equation](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D%5Cbegin%7BBmatrix%7Ddw/sw%20&%200%20&%20dx/sw%20%5C%5C0%20&%20dh/sh%20&%20dy/sh%20%5C%5C0%20&%200%20&%201%20%5C%5C%5Cend%7BBmatrix%7D*%5Cbegin%7BBmatrix%7Dpw/dw%20&%200%20&%20(dw-pw)/dw/2%20%5C%5C0%20&%20ph/dh%20&%20(dh-ph)/dh/2%20%5C%5C0%20&%200%20&%201%20%5C%5C%5Cend%7BBmatrix%7D)

equals to

![my equation](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D%5Cbegin%7BBmatrix%7Ddw/sw%20&%200%20&%20dx/sw%20%5C%5C0%20&%20dh/sh%20&%20dy/sh%20%5C%5C0%20&%200%20&%201%20%5C%5C%5Cend%7BBmatrix%7D*%5Cbegin%7BBmatrix%7Dpw/dw%20&%200%20&%200%20%5C%5C0%20&%20ph/dh%20&%200%20%5C%5C0%20&%200%20&%201%20%5C%5C%5Cend%7BBmatrix%7D*%5Cbegin%7BBmatrix%7D1%20&%200%20&%20-(1-dw/pw)/2%20%5C%5C0%20&%201%20&%20-(1-dh/ph)/2%20%5C%5C0%20&%200%20&%20%201%20%5C%5C%5Cend%7BBmatrix%7D)

### 'Full aspect' mode

(with left and right side blank)

![my equation](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D%5Cbegin%7BBmatrix%7Dpw*dh/ph/sw%20&%200%20&%20(1-pw*dh/ph/dw)*dw/dw/2&plus;dx/sw%20%5C%5C0%20&%20dh/sh%20&%20dy/sh%20%5C%5C0%20&%200%20&%201%20%5C%5C%5Cend%7BBmatrix%7D)

equals to

![my equation](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D%5Cbegin%7BBmatrix%7Ddw/sw%20&%200%20&%20dx/sw%20%5C%5C0%20&%20dh/sh%20&%20dy/sh%20%5C%5C0%20&%200%20&%201%20%5C%5C%5Cend%7BBmatrix%7D*%5Cbegin%7BBmatrix%7Dpw*dh/ph/dw%20&%200%20&%20(1-pw*dh/ph/dw)/2%20%5C%5C0%20&%201%20&%200%20%5C%5C0%20&%200%20&%201%20%5C%5C%5Cend%7BBmatrix%7D)

equals to

![my equation](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D%5Cbegin%7BBmatrix%7Ddw/sw%20&%200%20&%20dx/sw%20%5C%5C0%20&%20dh/sh%20&%20dy/sh%20%5C%5C0%20&%200%20&%201%20%5C%5C%5Cend%7BBmatrix%7D*%5Cbegin%7BBmatrix%7Dpw*dh/ph/dw%20&%200%20&%200%20%5C%5C0%20&%201%20&%200%20%5C%5C0%20&%200%20&%201%20%5C%5C%5Cend%7BBmatrix%7D*%5Cbegin%7BBmatrix%7D1%20&%200%20&%20(ph*dw/pw/dh-1)/2%20%5C%5C0%20&%201%20&%200%20%5C%5C0%20&%200%20&%20%201%20%5C%5C%5Cend%7BBmatrix%7D)

## Rotation

$ xrandr -o left

![my equation](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D%5Cbegin%7BBmatrix%7D0%20&%20-1%20&%201%20%5C%5C1%20&%200%20&%200%20%5C%5C0%20&%200%20&%201%20%5C%5C%5Cend%7BBmatrix%7D)

$ xrandr -o right

![my equation](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D%5Cbegin%7BBmatrix%7D0%20&%201%20&%200%20%5C%5C-1%20&%200%20&%201%20%5C%5C0%20&%200%20&%201%20%5C%5C%5Cend%7BBmatrix%7D)

$ xrandr -o inverted

![my equation](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D%5Cbegin%7BBmatrix%7D-1%20&%200%20&%201%20%5C%5C0%20&%20-1%20&%201%20%5C%5C0%20&%200%20&%201%20%5C%5C%5Cend%7BBmatrix%7D)

## Reflection

$ xrandr -x

![my equation](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D%5Cbegin%7BBmatrix%7D-1%20&%200%20&%201%20%5C%5C0%20&%201%20&%200%20%5C%5C0%20&%200%20&%201%20%5C%5C%5Cend%7BBmatrix%7D)

$ xrandr -y

![my equation](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D%5Cbegin%7BBmatrix%7D1%20&%200%20&%200%20%5C%5C0%20&%20-1%20&%201%20%5C%5C0%20&%200%20&%201%20%5C%5C%5Cend%7BBmatrix%7D)

## License

Copyright 2022 Igor An. Berezhnov (Vinnie)
